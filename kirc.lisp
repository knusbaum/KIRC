(in-package :kirc)

(eval-when (:compile-toplevel :load-toplevel)
  (declaim (optimize (speed 0) (safety 3) (debug 3))))

(define-application-frame kirc ()
  ((irc-connection :initform nil :accessor irc-connection)
   (channel-map :initform (make-hash-table :test #'equal :synchronized t) :accessor channel-map)
   (current-log :initform "main" :accessor current-log)
   (nick :initarg :nick :accessor nick))
  (:panes
   (buttons
    :clim-stream-pane
    :display-function 'display-buttons
    :display-time t)
   (main-display
    :application
    :display-function 'display-main
    :display-time t)
   (int :interactor))
  (:top-level (clim:default-frame-top-level :prompt 'kirc-prompt))
  (:layouts
   (default (horizontally ()
              (1/10 buttons)
              (9/10 (vertically ()
                      (99/100 main-display)
                      (1/100 int)))))))

(defun kirc-prompt (*standard-output* *application-frame*)
  (stream-increment-cursor-position *standard-output* 3 4)
  (write-string "KIRC > "))

;; Taken from BEIRC (thanks)
(defun make-fake-irc-message (message-type &key command arguments
                                             (source (nick *application-frame*))
                                             trailing-argument)
  (make-instance message-type
                 :received-time (get-universal-time)
                 :connection (irc-connection *application-frame*)
                 :arguments `(,@arguments ,trailing-argument)
                 :command command
                 :HOST "localhost"
                 :USER "localuser"
                 :SOURCE source))

(defun handle-input (frame stream)
  (handler-bind ((simple-completion-error #'(lambda (e) (return-from handle-input nil)))
                 (accelerator-gesture #'(lambda (gesture)
                                          (format *error-output* "Got Gesture: ~a~%" gesture))))
    (with-input-context ('command) (object)
        (with-command-table-keystrokes (*accelerator-gestures* (frame-command-table frame))
          (multiple-value-bind (gesture whatever) (read-gesture :stream stream :peek-p t)
            (if (eq gesture #\/)
                (progn
                  (read-gesture :stream stream)
                  (let ((command (accept 'command :stream stream :prompt nil :prompt-mode :raw)))
                    (format *error-output* "~a class: ~a~%" command (class-of command))
                    (apply #'funcall (car command) (cdr command))))
                (let ((out-msg (accept 'string :stream stream :prompt nil :prompt-mode :raw)))
                  (when (or (not (stringp out-msg)) (= (length out-msg) 0))
                    (format *error-output* "Not a string: ~a~%" out-msg)
                    (return-from handle-input nil))
                  (let ((fake-message (make-fake-irc-message 'cl-irc:irc-privmsg-message
                                                             :command "PRIVMSG"
                                                             :arguments (list (current-log frame))
                                                             :trailing-argument out-msg)))
                    (cl-irc:privmsg (irc-connection frame) (current-log frame)
                                    (format nil "~a~%" out-msg))
                    (deliver-message frame fake-message))))))))
  nil)

(defmethod read-frame-command ((frame kirc) &key (stream *standard-input*))
  (clim:with-input-editing (stream :input-sensitizer (lambda (x cont)
                                                       (declare (ignore x))
                                                       (funcall cont)))
    ;; Calling it like this so we can update the fn while running.
    (funcall 'handle-input frame stream)))

(defvar *presentation-types*
  '((cl-irc:irc-privmsg-message . irc-privmsg)))

(define-presentation-type irc-privmsg () :inherit-from '((string)
                                                         :description "irc-privmsg"))

(define-presentation-method present (irc-privmsg (type irc-privmsg) stream
                                                 (view textual-view)
                                                 &key acceptably)
  (declare (ignore acceptably))
  (multiple-value-bind
        (second minute hour)
      (decode-universal-time (cl-irc:received-time irc-privmsg))
    (let ((timestr (format nil "[~2,'0d:~2,'0d:~2,'0d]" hour minute second))
          (source (format nil "[~a]" (cl-irc:source irc-privmsg)))
          (msg (format nil " ~a~%" (second (cl-irc:arguments irc-privmsg)))))
      (present timestr 'string :sensitive nil)
      (with-drawing-options (stream :ink (color-for-nick source))
        (present source 'string :sensitive nil))
      (present msg 'string :sensitive nil))))
;    (format stream "[~2,'0d:~2,'0d:~2,'0d]~t[~a]: ~t~a~%"
;            hour minute second
;            (cl-irc:source irc-privmsg)
;            (second (cl-irc:arguments irc-privmsg)))))

(define-presentation-type irc-message () :inherit-from '((string)
                                                         :description "irc-message"))

(define-presentation-method present (irc-message (type irc-message) stream
                                                 (view textual-view)
                                                 &key acceptably)
  (declare (ignore acceptably))
;  (format stream "[~a][~a][~a][~a][~a][~a][~a][~a]~%"
;          (cl-irc:source irc-message)
;          (cl-irc:user irc-message)
;          (cl-irc:host irc-message)
;          (cl-irc:command irc-message)
;          (cl-irc:arguments irc-message)
;          (cl-irc:connection irc-message)
;          (cl-irc:received-time irc-message)
;          (cl-irc:raw-message-string irc-message)))
  (with-drawing-options (stream :ink +burlywood+)
    (format stream "~t~t~t~a~%" (cl-irc:raw-message-string irc-message))))

(defun get-channel (kirc-app chan-name)
  (or
   (gethash chan-name (channel-map kirc-app))
   (progn
     (setf (gethash chan-name (channel-map kirc-app))
           (make-instance 'channel))
     (format *error-output* "Creating new button~%")
     (perform-button-redisplay kirc-app)
     (get-channel kirc-app chan-name))))

(defun get-log (kirc-app logname)
  (chan-log (get-channel kirc-app logname)))

(defun get-current-log (kirc-app)
  (get-log kirc-app (current-log kirc-app)))


;;; Redisplay machinery
(defclass main-redisplay-event (window-manager-event)
  ((item :initarg :item :accessor item)))

(defmethod handle-event ((frame kirc) (event main-redisplay-event))
  (format *error-output* "Handling redisplay event: ~a~%" event)
  (with-application-frame (frame)
    (format *error-output* "Main display: ~a~%" (find-pane-named frame 'main-display))
    (setf (pane-needs-redisplay (find-pane-named frame 'main-display)) t)
    (redisplay-frame-pane frame (find-pane-named frame 'main-display))))

(defun perform-main-redisplay (kirc-app)
  (format *error-output* "perform-main-redisplay~%")
  (queue-event (frame-top-level-sheet kirc-app)
               (make-instance 'main-redisplay-event
                              :sheet kirc-app)))

(defclass button-redisplay-event (window-manager-event)
  ((item :initarg :item :accessor item)))

(defmethod handle-event ((frame kirc) (event button-redisplay-event))
  (format *error-output* "Handling redisplay event: ~a~%" event)
  (with-application-frame (frame)
    (format *error-output* "Button display: ~a~%" (find-pane-named frame 'buttons))
    (setf (pane-needs-redisplay (find-pane-named frame 'buttons)) t)
    (redisplay-frame-pane frame (find-pane-named frame 'buttons))))

(defun perform-button-redisplay (kirc-app)
  (format *error-output* "perform-button-redisplay~%")
  (queue-event (frame-top-level-sheet kirc-app)
               (make-instance 'button-redisplay-event
                              :sheet kirc-app)))


(defclass message-event (window-manager-event)
  ((item :initarg :item :accessor item)))

(defun deliver-message (kirc-app message)
  (queue-event (frame-top-level-sheet kirc-app)
               (make-instance 'message-event
                              :sheet kirc-app
                              :item message)))

(defun present-message (msg &optional (stream t))
  (let* ((msg-class (class-name (class-of msg)))
         (presentation-type (cdr (assoc msg-class *presentation-types*))))
    (if presentation-type
        (present msg presentation-type :stream stream :sensitive nil)
        (present msg 'irc-message :stream stream :sensitive nil))))

(defmethod handle-event ((frame kirc) (event message-event))
  (with-application-frame (frame)
    (let ((stream (find-pane-named frame 'main-display)))
      (let* ((msg (item event))
             (args (cl-irc:arguments msg)))
        (if (and (first args) (eq (elt (first args) 0) #\#))
            (progn
              (log-append (get-log frame (first args)) msg)
              (if (equal (first args) (current-log frame))
                  (present-message msg stream)
                  (mark-channel (get-channel frame (first args)) msg)))
            (progn
              (log-append (get-log frame "main") msg)
              (if (equal "main" (current-log frame))
                  (present-message msg stream)
                  (mark-channel (get-channel frame "main") msg))))
        (perform-button-redisplay frame)))))

(defun display-main (frame stream)
  (format *error-output* "Beginning Redisplay for ~a.~%" (current-log frame))
;  (mapcar (lambda (c)
;            (format *error-output* "~a~%" c)
;            (with-drawing-options (stream :ink c)
;              (format t "~a~%" c)))
;          *color-list*)
  (loop for msg in (head (get-current-log frame))
     do (present-message msg stream))
  (format *error-output* "Ending Redisplay.~%"))

(defun switch-to-channel (kirc-app channel)
  (setf (current-log kirc-app) channel)
  (clear-flags (get-channel kirc-app channel))
  (perform-button-redisplay kirc-app)
  (perform-main-redisplay kirc-app))

(defun display-buttons (frame stream)
  (format *error-output* "Beginning Redisplay of buttons.~%")
  (with-application-frame (app)
    (with-look-and-feel-realization ((find-frame-manager) frame)
      ;(format stream "(m -> New Message)~%(e -> New Event)~%")
      (with-output-as-gadget (stream)
        (make-pane
         'vrack-pane
         :contents
         (loop for k being the hash-keys in (channel-map app)
            collecting
              (let ((channel (get-channel frame k)))
                (format *error-output* "Adding button with color ~a~%" k)
                (make-pane 'push-button :label k ;(make-label channel k)
                           :text-style (clim:make-text-style :sans-serif :bold :normal)
                           :foreground (make-label-color channel)
                           :activate-callback (let ((k k))
                                                (lambda (b)
                                                  (declare (ignore b))
                                                  (switch-to-channel frame k)))))))))))

(defun run-irc-conn (kirc conn channels)
  (format t "KIRC: ~a~%" kirc)
  (unwind-protect
       (progn
         (mapcar (lambda (c) (cl-irc:join conn c)) channels)
         (loop for msg = (handler-case
                             (cl-irc:read-irc-message conn)
                           (error (e)
                             (format *error-output* "Error reading IRC message: ~a~%" e)
                             :error))
            while msg
            do (when (not (eq msg :error))
                 (cl-irc:irc-message-event conn msg)
                 (deliver-message kirc msg))))
    (cl-irc:quit conn "BYE")
    (format t "Shut down client.~%")))

(defvar *log* nil)
(defvar *app* nil)
(defvar *appthread* nil)

(defun start-kirc-nothread (nickname password &key
                                                (server "chat.freenode.net")
                                                (port 6697)
                                                (connection-security :ssl)
                                                channels)
  (let* ((kirc (make-application-frame 'kirc :nick nickname :width 1024 :height 768))
         (conn (cl-irc:connect
                :nickname nickname :password password
                :server server
                :port port
                :connection-security connection-security))
         (irc-conn-thread (clim-sys:make-process (lambda () (run-irc-conn kirc conn channels))
                                                 :name "irc-conn-thread")))
    (setf (irc-connection kirc) conn)
    (setf *app* kirc)
    (setf *log* (current-log kirc))
    (unwind-protect
         (run-frame-top-level kirc)
      (clim-sys:destroy-process irc-conn-thread))))

(defun start-kirc (nickname password &key
                                       (server "chat.freenode.net")
                                       (port 6697)
                                       (connection-security :ssl)
                                       channels)
  #.(format nil
  "Launch the kirc client.~@
   (start-kirc nickname password &key server port connection-security channels)~@
   ~@
   Required:~@
   nickname - nick you want to use~@
   password - used to authenticate to the network~@
   ~@
   Keyword:~@
   server - An IRC server host string. Defaults to freenode.~@
   port - Integer port for the server~@
   connection-security - As in cl-irc:connect. :ssl by default~@
   channels - list of channels to join after connecting.~@
   ~@
   Interactor:~@
   Current available commands are:~@
   /Join (Channel)~@
   /Part (Channel [current channel])~@
   /Nick (New Nick)")
  (setf *appthread*
        (clim-sys:make-process (lambda () (start-kirc-nothread
                                           nickname password
                                           :server server
                                           :port port
                                           :connection-security connection-security
                                           :channels channels))
                               :name "KIRC-MAIN")))
