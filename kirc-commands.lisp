(in-package :kirc)

;(define-kirc-command (com-paste :name t :keystroke (#\v :control)) ()
;  (format *error-output* "PASTE!~%"))

(define-kirc-command (com-join :name t) ((channel 'string :prompt "Channel"))
  (with-application-frame (frame)
    (format *error-output* "Join: ~a~%"
            (cl-irc:join (irc-connection frame) channel))))

(define-kirc-command (com-part :name t) ((channel 'string :prompt "Channel" :default nil))
  (with-application-frame (kirc-app)
    (let ((to-part (or channel (current-log kirc-app))))
      (format *error-output* "Part: ~a~%"
              (cl-irc:part (irc-connection kirc-app) to-part "Bye."))
      (remhash to-part (channel-map kirc-app))
      (when (equal to-part (current-log kirc-app))
        (with-hash-table-iterator (i (channel-map kirc-app))
          (multiple-value-bind (_1 chan _2) (i)
            (switch-to-channel kirc-app chan)))))))

(define-kirc-command (com-nick :name t) ((nick 'string :prompt "New Nick"))
  (with-application-frame (kirc-app)
    (cl-irc:nick (irc-connection kirc-app) nick)))
