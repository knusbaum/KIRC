(defpackage :kirc-system
  (:use :cl :asdf))
(in-package :kirc-system)

(asdf:defsystem :kirc
  :name "kirc"
  :description "Simple, graphical IRC client built on top of CLIM"
  :license "BSD"
  :author "Kyle Nusbaum"
  :depends-on (:mcclim :cl-irc :cl+ssl)
  :components ((:file "package")
               (:file "kirc-channel")
               (:file "kirc-color")
               (:file "kirc"
                      :depends-on ("kirc-channel" "kirc-color"))
               (:file "kirc-commands"
                      :depends-on ("kirc"))))

