#!/usr/bin/env sbcl --script

(load "~/.sbclrc")
(ql:quickload '(kirc clim-debugger clim-pw))

(setf *debugger-hook* #'clim-debugger::debugger)

(defun main ()
  (clim-debugger:with-debugger
    (multiple-value-bind (username pw)
        (clim-pw:get-password :with-username t)
      (kirc:start-kirc-nothread username pw))))

(sb-ext:enable-debugger)

(save-lisp-and-die "kirc" :toplevel #'main :executable t :compression t)
