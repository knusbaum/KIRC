
(defpackage :kirc
  (:use :clim-lisp :clim :cl+ssl)
  (:export
   start-kirc
   start-kirc-nothread))
