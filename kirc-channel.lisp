(in-package :kirc)

(defclass concurrent-log ()
  ((head :initform nil :accessor head)
   (tail :initform nil :accessor tail)
   (lock :initform (clim-sys:make-recursive-lock) :accessor lock)))

(defgeneric (setf head) (value log))
(defmethod (setf head) (value (log concurrent-log))
  (clim-sys:with-recursive-lock-held ((lock log))
    (setf (slot-value log 'head) value)))

(defgeneric (setf tail) (value log))
(defmethod(setf tail) (value (log concurrent-log))
  (clim-sys:with-recursive-lock-held ((lock log))
    (setf (slot-value log 'tail) value)))

(defgeneric head (log))
(defmethod head ((log concurrent-log))
  (clim-sys:with-recursive-lock-held ((lock log))
    (slot-value log 'head)))

(defgeneric tail (log))
(defmethod tail ((log concurrent-log))
  (clim-sys:with-recursive-lock-held ((lock log))
    (slot-value log 'tail)))

(defgeneric log-append (log item))
(defmethod log-append ((log concurrent-log) item)
  (clim-sys:with-recursive-lock-held ((lock log))
    (let ((elem (cons item nil)))
      (if (eq (tail log) nil)
          ;; First entry
          (progn
            (setf (head log) elem)
            (setf (tail log) elem))
          ;; add to tail
          (progn
            (setf (cdr (tail log)) elem)
            (setf (tail log) elem))))))

(defclass channel ()
  ((chan-log :initform (make-instance 'concurrent-log) :accessor chan-log)
   (has-unread :initform nil :accessor has-unread)
   (has-event :initform nil :accessor has-event)))

(defun make-label (channel chan-name)
  (format *error-output* "Making label for channel: ~a~%" chan-name)
  (let ((flags nil))
    (when (has-unread channel)
      (format *error-output* "Has unread!~%")
      (setf flags (append flags '("m"))))
    (when (has-event channel)
      (format *error-output* "Has event!~%")
      (setf flags (append flags '("e"))))
    (format nil "~a (~{~a~})" chan-name flags)))

(defun make-label-color (channel)
  (cond
    ((has-unread channel) clim:+orange+)
    ((has-event channel) clim:+blue+)
    (t clim:+gray+)))

(defun mark-channel (channel msg)
  (if (eq (class-name (class-of msg)) 'cl-irc:irc-privmsg-message)
      (progn
        (format *error-output* "Setting unread for ~a~%" channel)
        (setf (has-unread channel) t))
      (progn
        (format *error-output* "Setting event for ~a~%" channel)
        (setf (has-event channel) t))))

(defun clear-flags (channel)
  (format *error-output* "Clearing flags!~%")
  (setf (has-unread channel) nil)
  (setf (has-event channel) nil))
