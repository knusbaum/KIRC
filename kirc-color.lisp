(in-package :kirc)

(defvar *color-list*
  (list
;   clim:+ALICE-BLUE+
;   clim:+ALICEBLUE+
;   clim:+ANTIQUE-WHITE+
;   clim:+AQUAMARINE+
;   clim:+AZURE+
;   clim:+BEIGE+
;   clim:+BISQUE+
;   clim:+BLACK+
;   clim:+BLANCHED-ALMOND+
   clim:+BLUE+
   clim:+BLUE-VIOLET+
   clim:+BROWN+
   clim:+BURLYWOOD+
   clim:+CADET-BLUE+
   clim:+CADETBLUE+
   clim:+CHARTREUSE+
   clim:+CHOCOLATE+
   clim:+CORAL+
   clim:+CORNFLOWER-BLUE+
;   clim:+CORNSILK+
   clim:+CYAN+
   clim:+DARK-BLUE+
   clim:+DARK-CYAN+
   clim:+DARK-GOLDENROD+
   clim:+DARK-GRAY+
   clim:+DARK-GREEN+
   clim:+DARK-KHAKI+
   clim:+DARK-MAGENTA+
   clim:+DARK-OLIVE-GREEN+
   clim:+DARK-ORANGE+
   clim:+DARK-ORCHID+
   clim:+DARK-RED+
   clim:+DARK-SALMON+
   clim:+DARK-SEA-GREEN+
   clim:+DARK-SLATE-BLUE+
   clim:+DARK-SLATE-GRAY+
   clim:+DARK-TURQUOISE+
   clim:+DARK-VIOLET+
   clim:+DARKBLUE+
   clim:+DARKCYAN+
   clim:+DARKGOLDENROD+
   clim:+DEEP-PINK+
   clim:+DEEP-SKY-BLUE+
   clim:+DIM-GRAY+
   clim:+DODGER-BLUE+
   clim:+FIREBRICK+
;   clim:+FLORAL-WHITE+
   clim:+FOREST-GREEN+
;   clim:+GAINSBORO+
;   clim:+GHOST-WHITE+
   clim:+GOLD+
   clim:+GOLDENROD+
;   clim:+GRAY+
   clim:+GREEN+
   clim:+GREEN-YELLOW+
;   clim:+HONEYDEW+
   clim:+HOT-PINK+
   clim:+INDIAN-RED+
;   clim:+IVORY+
;   clim:+KHAKI+
;   clim:+LAVENDER+
;   clim:+LAVENDER-BLUSH+
   clim:+LAWN-GREEN+
;   clim:+LEMON-CHIFFON+
;   clim:+LIGHT-BLUE+
;   clim:+LIGHT-CORAL+
;   clim:+LIGHT-CYAN+
;   clim:+LIGHT-GOLDENROD+
;   clim:+LIGHT-GOLDENROD-YELLOW+
;   clim:+LIGHT-GRAY+
;   clim:+LIGHT-GREEN+
;   clim:+LIGHT-GREY+
;   clim:+LIGHT-PINK+
;   clim:+LIGHT-SALMON+
;   clim:+LIGHT-SEA-GREEN+
;   clim:+LIGHT-SKY-BLUE+
;   clim:+LIGHT-SLATE-BLUE+
;   clim:+LIGHT-SLATE-GRAY+
;   clim:+LIGHT-SLATE-GREY+
;   clim:+LIGHT-STEEL-BLUE+
;   clim:+LIGHT-YELLOW+
   clim:+LIME-GREEN+
;   clim:+LINEN+
   clim:+MAGENTA+
   clim:+MAROON+
   clim:+MEDIUM-AQUAMARINE+
   clim:+MEDIUM-BLUE+
   clim:+MEDIUM-ORCHID+
   clim:+MEDIUM-PURPLE+
   clim:+MEDIUM-SEA-GREEN+
   clim:+MEDIUM-SLATE-BLUE+
   clim:+MEDIUM-SPRING-GREEN+
   clim:+MEDIUM-TURQUOISE+
   clim:+MEDIUM-VIOLET-RED+
   clim:+MIDNIGHT-BLUE+
;   clim:+MINT-CREAM+
;   clim:+MISTY-ROSE+
;   clim:+MOCCASIN+
;   clim:+NAVAJO-WHITE+
   clim:+NAVY+
;   clim:+OLD-LACE+
   clim:+OLIVE-DRAB+
   clim:+ORANGE+
   clim:+ORANGE-RED+
   clim:+ORCHID+
;   clim:+PALE-GOLDENROD+
;   clim:+PALE-GREEN+
;   clim:+PALE-TURQUOISE+
;   clim:+PALE-VIOLET-RED+
;   clim:+PAPAYA-WHIP+
;   clim:+PEACH-PUFF+
   clim:+PERU+
   clim:+PINK+
;   clim:+PLUM+
;   clim:+POWDER-BLUE+
   clim:+PURPLE+
   clim:+RED+
   clim:+ROSY-BROWN+
   clim:+ROYAL-BLUE+
   clim:+SADDLE-BROWN+
   clim:+SALMON+
   clim:+SANDY-BROWN+
   clim:+SEA-GREEN+
;   clim:+SEASHELL+
   clim:+SIENNA+
   clim:+SKY-BLUE+
   clim:+SLATE-BLUE+
   clim:+SLATE-GRAY+
;   clim:+SNOW+
   clim:+SPRING-GREEN+
   clim:+STEEL-BLUE+
   clim:+TAN+
   clim:+THISTLE+
   clim:+TOMATO+
   clim:+TURQUOISE+
   clim:+VIOLET+
   clim:+VIOLET-RED+
;   clim:+WHEAT+
;   clim:+YELLOW+
   clim:+YELLOW-GREEN+))

(defvar *nick-color-table* (make-hash-table :test #'equal))

(defun color-for-nick (nick)
  (let ((color (gethash nick *nick-color-table*)))
    (when (eql color nil)
      (setf color (elt *color-list* (random (length *color-list*)) ))
      (setf (gethash nick *nick-color-table*) color))
    color))
